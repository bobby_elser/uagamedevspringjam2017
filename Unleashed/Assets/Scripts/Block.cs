﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour
{
    public GameObject NorthWall;
    public GameObject SouthWall;
    public GameObject EastWall;
    public GameObject WestWall;
    public GameObject Floor;
    public GameObject Ceiling;

    public int proximityToEndBlock = -1;
    public int travelDistanceToEndBlock = -1;

    public bool northOpen = false;
    public bool southOpen = false;
    public bool eastOpen = false;
    public bool westOpen = false;

    public bool alreadyBuilt = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OpenNorth()
    {
        NorthWall.SetActive(false);
        northOpen = true;
    }

    public void OpenSouth()
    {
        SouthWall.SetActive(false);
        southOpen = true;
    }

    public void OpenEast()
    {
        EastWall.SetActive(false);
        eastOpen = true;
    }

    public void OpenWest()
    {
        WestWall.SetActive(false);
        westOpen = true;
    }
}
