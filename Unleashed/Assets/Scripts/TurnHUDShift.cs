﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnHUDShift : MonoBehaviour
{
    public float maxShift = 0.3f;
    public float maxSpeed = 0.05f;
    public float smoothing = 10;

    private Vector3 restPosition;
    private Queue<float> targetShifts;

    void Start()
    {
        restPosition = transform.localPosition;
        targetShifts = new Queue<float>();
    }

    void Update()
    {
        float input = Input.GetAxis("Mouse X");
        float rawTargetShift = restPosition.x - input * maxShift; //shift in opposite direction of turn
        pushNewTargetShift(rawTargetShift);
        float targetShift = getAverageTargetShift();
        Debug.Log("shift " + targetShift);
        Vector3 currentPosition = transform.localPosition;
        if (targetShift < currentPosition.x) //need to shift left
        {
            currentPosition.x -= Mathf.Min(maxSpeed, currentPosition.x - targetShift);
        }
        else if (targetShift > currentPosition.x) //need to shift right
        {
            currentPosition.x += Mathf.Min(maxSpeed, Mathf.Abs(currentPosition.x - targetShift));
        }
        transform.localPosition = currentPosition;
    }

    private void pushNewTargetShift(float newShift)
    {
        targetShifts.Enqueue(newShift);
        while (targetShifts.Count > smoothing)
        {
            targetShifts.Dequeue();
        }
    }

    private float getAverageTargetShift()
    {
        float average = 0f;
        foreach (float target in targetShifts)
        {
            average += target;
        }
        average /= targetShifts.Count;
        return average;
    }
}
