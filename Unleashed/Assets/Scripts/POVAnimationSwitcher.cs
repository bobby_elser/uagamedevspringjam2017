﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class POVAnimationSwitcher : MonoBehaviour 
{
    private Animator anim;

	void Start () 
	{
        anim = GetComponent<Animator>();
	}
	
	void Update () 
	{
		if (Mathf.Approximately(Input.GetAxis("Horizontal"), 0) && Mathf.Approximately(Input.GetAxis("Vertical"), 0))
        {
            anim.SetBool("isWalking", false);
        }
        else
        {
            anim.SetBool("isWalking", true);
        }
    }
}
