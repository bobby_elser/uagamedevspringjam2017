﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Level))]
public class LevelEditor : Editor
{
    public override void OnInspectorGUI()
    {
        bool rebuilt = false;
        DrawDefaultInspector();
        Level level = (Level)target;
        if (GUILayout.Button("Clear"))
        {
            level.clearMap();
        }
        if (GUILayout.Button("Rebuild"))
        {
            level.clearMap();
            level.initMap();
            bool success = level.tryBuildMaze();
            if (!success)
            {
                GUIStyle warningStyle = new GUIStyle();
                warningStyle.normal.textColor = Color.red;
                GUILayout.Label("Rebuild failed - cannot reach exit", warningStyle);
            }
            rebuilt = true;
        }
        if (GUILayout.Button("Copy build output"))
        {
            EditorGUIUtility.systemCopyBuffer = level.lastBuildLog;
        }
    }
}