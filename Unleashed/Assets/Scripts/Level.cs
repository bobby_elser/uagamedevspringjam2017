﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class Level : MonoBehaviour 
{
    public Block blockPrefab;
    public GameObject playerPrefab;
    public GameObject entrancePrefab;
    public GameObject exitPrefab;
    public int minDistanceToEnd = 3;
    public float branchProbability = 0.3f;
    public float loopProbability = 0.02f;

    [HideInInspector]
    public string lastBuildLog;

    public int width;
    public int length;

    private Block[][] blocks;
    private GameObject player;
    private GameObject entrance;
    private GameObject exit;

	void Start () 
	{
        bool exitIsReachable = false;
        while (!exitIsReachable)
        {
            clearMap();
            initMap();
            exitIsReachable = tryBuildMaze();
        }
	}
	
	void Update () 
	{
		
	}

    public void clearMap()
    {
        if (blocks != null)
        {
            for (int x = 0; x < width; x++)
            {
                for (int z = 0; z < length; z++)
                {
                    DestroyImmediate(blocks[x][z].gameObject);
                    blocks[x][z] = null;
                }
                blocks[x] = null;
            }
            blocks = null;
        }

        if (entrance != null) DestroyImmediate(entrance);
        if (exit != null) DestroyImmediate(exit);
        if (player != null) DestroyImmediate(player);
    }

    public void initMap()
    {
        blocks = new Block[width][];

        for (int x = 0; x < width; x++)
        {
            blocks[x] = new Block[length];

            for (int z = 0; z < length; z++)
            {
                Block newBlock = Instantiate(blockPrefab, this.transform);

                newBlock.transform.localPosition = new Vector3(x, 0, z);

                //if (x != 0) newBlock.WestWall.SetActive(false);
                //if (x != width - 1) newBlock.EastWall.SetActive(false);
                //if (z != 0) newBlock.SouthWall.SetActive(false);
                //if (z != length - 1) newBlock.NorthWall.SetActive(false);

                blocks[x][z] = newBlock;
            }
        }
    }

    private GridPosition setupStartPosition()
    {
        GridPosition startPosition = new GridPosition(width/2, length/2);
        Block startBlock = blocks[startPosition.x][startPosition.z];
        player = Instantiate(playerPrefab, this.transform);
        player.transform.localPosition = startBlock.transform.localPosition;
        startBlock.alreadyBuilt = true;
        entrance = Instantiate(entrancePrefab, this.transform);
        entrance.transform.localPosition = startBlock.transform.position;
        return startPosition;
    }

    private GridPosition setupEndPosition(GridPosition startPosition)
    {
        int endX = Random.Range(0, width - minDistanceToEnd);
        if (endX >= startPosition.x) endX += minDistanceToEnd;
        int endZ = Random.Range(0, length - minDistanceToEnd);
        if (endZ >= startPosition.z) endZ += minDistanceToEnd;

        //int endX = Random.Range(0, Mathf.Max(1, width - 2 * minDistanceToEnd));
        //if (endX > startPosition.x - minDistanceToEnd) endX += minDistanceToEnd;
        //if (endX < startPosition.x + minDistanceToEnd) endX += minDistanceToEnd;

        //int endZ = Random.Range(0, Mathf.Max(1, length - minDistanceToEnd));
        //if (endZ > startPosition.z - minDistanceToEnd) endZ += minDistanceToEnd;
        //if (endZ < startPosition.z + minDistanceToEnd) endZ += minDistanceToEnd;

        GridPosition endPosition = new GridPosition(endX, endZ);
        exit = Instantiate(exitPrefab, this.transform);
        exit.transform.localPosition = blocks[endX][endZ].transform.position;
        return endPosition;
    }

    public bool tryBuildMaze()
    {
        GridPosition startPosition = setupStartPosition();
        GridPosition endPosition = setupEndPosition(startPosition);

        //int[][] distanceToEnd = buildDistanceGrid(endPosition);

        StringBuilder mapText = new StringBuilder("Starting to build the maze.");
        GridPosition currentPosition = startPosition;
        LinkedList<GridPosition> branchCandidates = new LinkedList<GridPosition>();
        bool exitIsReachable = false;
        do
        {
#if UNITY_EDITOR
            renderGridAsText(mapText, startPosition, endPosition, currentPosition);
#endif
            mapText.AppendLine();

            if (currentPosition.x == endPosition.x && currentPosition.z == endPosition.z)
            {
                mapText.AppendLine("End reached.");
                exitIsReachable = true;
                currentPosition = branchCandidates.First.Value;
                branchCandidates.RemoveFirst();
                continue;
            }

            if (branchCandidates.Count > 1 && Random.value < branchProbability)
            {
                mapText.AppendLine("Decided to stop building this path.");
                branchCandidates.AddLast(currentPosition); //Revisit the current position later
                currentPosition = branchCandidates.First.Value;
                branchCandidates.RemoveFirst();
                continue;
            }

            List<GridPosition> eligibleNeighbors = new List<GridPosition>();
            GridPosition neighborToCheck;

            //west neighbor:
            neighborToCheck = new GridPosition(currentPosition.x - 1, currentPosition.z);
            if (neighborToCheck.x >= 0 && (!blocks[neighborToCheck.x][neighborToCheck.z].alreadyBuilt || Random.value < loopProbability))
            {
                eligibleNeighbors.Add(neighborToCheck);
                mapText.AppendLine("West neighbor eligible for building.");
            }

            //east neighbor:
            neighborToCheck = new GridPosition(currentPosition.x + 1, currentPosition.z);
            if (neighborToCheck.x < width && (!blocks[neighborToCheck.x][neighborToCheck.z].alreadyBuilt || Random.value < loopProbability))
            {
                eligibleNeighbors.Add(neighborToCheck);
                mapText.AppendLine("East neighbor eligible for building.");
            }

            //south neighbor:
            neighborToCheck = new GridPosition(currentPosition.x, currentPosition.z - 1);
            if (neighborToCheck.z >= 0 && (!blocks[neighborToCheck.x][neighborToCheck.z].alreadyBuilt || Random.value < loopProbability))
            {
                eligibleNeighbors.Add(neighborToCheck);
                mapText.AppendLine("South neighbor eligible for building.");
            }

            //north neighbor:
            neighborToCheck = new GridPosition(currentPosition.x, currentPosition.z + 1);
            if (neighborToCheck.z < length && (!blocks[neighborToCheck.x][neighborToCheck.z].alreadyBuilt || Random.value < loopProbability))
            {
                eligibleNeighbors.Add(neighborToCheck);
                mapText.AppendLine("North neighbor eligible for building.");
            }

            if (eligibleNeighbors.Count > 0)
            {
                GridPosition positionToBuild = eligibleNeighbors[Random.Range(0, eligibleNeighbors.Count - 1)];
                Block blockToBuild = blocks[positionToBuild.x][positionToBuild.z];
                Block currentBlock = blocks[currentPosition.x][currentPosition.z];
                if (positionToBuild.x > currentPosition.x) // next block is east
                {
                    mapText.AppendLine("Building east.");
                    currentBlock.OpenEast();
                    blockToBuild.OpenWest();
                }
                else if (positionToBuild.x < currentPosition.x) // next block is west
                {
                    mapText.AppendLine("Building west.");
                    currentBlock.OpenWest();
                    blockToBuild.OpenEast();
                }
                else if (positionToBuild.z > currentPosition.z) // next block is north
                {
                    mapText.AppendLine("Building north.");
                    currentBlock.OpenNorth();
                    blockToBuild.OpenSouth();
                }
                else if (positionToBuild.z < currentPosition.z) // next block is south
                {
                    mapText.AppendLine("Building south.");
                    currentBlock.OpenSouth();
                    blockToBuild.OpenNorth();
                }
                else
                {
                    //something went very wrong, pretending this isn't possible until I regret it
                }

                if (blockToBuild.alreadyBuilt) //this is a loop, treat it like a random stop
                {
                    mapText.AppendLine("Built a loop.");
                    branchCandidates.AddLast(currentPosition); //Revisit the current position later
                    currentPosition = branchCandidates.First.Value;
                    branchCandidates.RemoveFirst();
                    continue;
                }
                else
                {
                    blockToBuild.alreadyBuilt = true;
                    branchCandidates.AddFirst(currentPosition);
                    currentPosition = positionToBuild;
                }
            }
            else // No unbuilt neighbors, go back to branch
            {
                mapText.AppendLine("No unbuilt neighbors.");
                currentPosition = branchCandidates.Last.Value;
                branchCandidates.RemoveLast();
            }
        }
        while (branchCandidates.Count > 0);

        lastBuildLog = mapText.ToString();

        return exitIsReachable;
    }

    private void renderGridAsText(StringBuilder sb, GridPosition startPosition, GridPosition endPosition, GridPosition currentPosition)
    {
        sb.AppendLine();
        sb.AppendLine();
        for (int z = length - 1; z >= 0; z--) //must draw from top to bottom, row by row not column by column
        {
            for (int x = 0; x < width; x++) //render north edge of row
            {
                sb.Append("*"); //northwest corner
                sb.Append((blocks[x][z].northOpen) ? "  " : "--");
            }
            sb.AppendLine("*"); //northeast corner of row

            for (int x = 0; x < width; x++) //render center of row
            {
                sb.Append((blocks[x][z].westOpen) ? " " : "|");
                if (x == startPosition.x && z == startPosition.z)
                    sb.Append("SS");
                else if (x == endPosition.x && z == endPosition.z)
                    sb.Append("XX");
                else if (x == currentPosition.x && z == currentPosition.z)
                    sb.Append("()");
                else
                    sb.Append("  ");
            }
            sb.AppendLine((blocks[width-1][z].eastOpen) ? " " : "|"); //east end of row

            if (z == 0) //only render south edge if rendering bottom row
            {
                for (int x = 0; x < width; x++)
                {
                    sb.Append("*"); //southwest corner
                    sb.Append((blocks[x][z].southOpen) ? "  " : "--");
                }
                sb.Append("*"); //southeast corner of row
            }
        }
    }

    private int[][] buildDistanceGrid(GridPosition endPosition)
    {
        int[][] distanceToEnd = new int[width][];
        for (int x = 0; x < width; x++)
        {
            distanceToEnd[x] = new int[length];
            for (int z = 0; z < length; z++)
            {
                distanceToEnd[x][z] = -1;
            }
        }
        Queue<GridPosition> positionsToEvaluate = new Queue<GridPosition>();
        positionsToEvaluate.Enqueue(endPosition);
        while (positionsToEvaluate.Count > 0)
        {
            GridPosition position = positionsToEvaluate.Dequeue();
            int distanceOfClosestNeighbor = -1;
            if (position.x > 0) //there's a neighbor to the left/west
                distanceOfClosestNeighbor = evaluateNeighborForDistance(new GridPosition(position.x - 1, position.z), distanceToEnd, positionsToEvaluate, distanceOfClosestNeighbor);
            if (position.x < width - 1) //there's a neighbor to the right/east
                distanceOfClosestNeighbor = evaluateNeighborForDistance(new GridPosition(position.x + 1, position.z), distanceToEnd, positionsToEvaluate, distanceOfClosestNeighbor);
            if (position.z > 0) //there's a neighbor to the bottom/south
                distanceOfClosestNeighbor = evaluateNeighborForDistance(new GridPosition(position.x, position.z - 1), distanceToEnd, positionsToEvaluate, distanceOfClosestNeighbor);
            if (position.z < length - 1) //there's a neighbor to the top/north
                distanceOfClosestNeighbor = evaluateNeighborForDistance(new GridPosition(position.x, position.z + 1), distanceToEnd, positionsToEvaluate, distanceOfClosestNeighbor);
            if (distanceOfClosestNeighbor < 0) //No neighbors have been evaluated yet, position is end
            {
                distanceToEnd[position.x][position.z] = 0;
            }
            else
            {
                distanceToEnd[position.x][position.z] = distanceOfClosestNeighbor + 1;
            }
        }
        return distanceToEnd;
    }

    private int evaluateNeighborForDistance(GridPosition neighbor, int[][] distanceToEnd, Queue<GridPosition> positionsToEvaluate, int distanceOfClosestNeighbor)
    {
        int neighborDistance = distanceToEnd[neighbor.x][neighbor.z];
        if (neighborDistance < 0) //neighbor hasn't been evaluated yet, add it to the queue
        {
            positionsToEvaluate.Enqueue(neighbor);
        }
        else if (distanceOfClosestNeighbor < 0 || neighborDistance < distanceOfClosestNeighbor)
        {
            return neighborDistance;
        }
        //else neighbor's been evaluated already but it's not the closest neighbor to the end
        return distanceOfClosestNeighbor;
    }
}

class GridPosition
{
    public int x;
    public int z;

    public GridPosition(int x, int z)
    {
        this.x = x;
        this.z = z;
    }

    public GridPosition(Vector3 vector)
    {
        this.x = (int)Mathf.Round(vector.x);
        this.z = (int)Mathf.Round(vector.z);
    }
}