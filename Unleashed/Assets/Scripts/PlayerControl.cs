﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerControl : MonoBehaviour 
{
    public float walkForce = 10f;
    public float walkSpeed;
    public float turnSpeed;

    private Rigidbody rb;

	void Start () 
	{
        rb = GetComponent<Rigidbody>();
	}
	
	void Update () 
	{
        Vector3 walkInput = new Vector3();
        walkInput.x = Input.GetAxis("Horizontal");
        walkInput.z = Input.GetAxis("Vertical");
        Vector3.ClampMagnitude(walkInput, 1.0f);

        //transform.Translate(walkInput * walkSpeed * Time.deltaTime);
        rb.AddRelativeForce(walkInput * walkForce);
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, walkSpeed);

        transform.Rotate(0, Input.GetAxis("Mouse X") * turnSpeed, 0);
    }
}
